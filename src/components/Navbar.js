import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Navbar extends Component {
  render() {
    return (
      <header style={headerStyle}>
        <h1>FoodApp</h1>
        <Link style={linkStyle} to="/">
          Menu
        </Link>{' '}
        |{' '}
        <Link style={linkStyle} to="/bills">
          Bills
        </Link>
      </header>
    );
  }
}
const headerStyle = {
  background: '#333',
  color: '#fff',
  textAlign: 'center',
  padding: '10px',
};

const linkStyle = {
  color: '#fff',
  textDecoration: 'none',
};

export default Navbar;
