import React, { Component } from 'react';

class Menu extends Component {
  render() {
    const { foodItem, handleClick } = this.props;
    return (
      <div className="menu-container">
        {foodItem.map((data) => (
          <div className="item-container" key={data.id}>
            <p>{data.item}</p>
            <p>{data.price} Rs</p>
            <button onClick={(id) => handleClick(data.id)}>BUY</button>
          </div>
        ))}
      </div>
    );
  }
}

export default Menu;
