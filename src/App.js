import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Menu from './components/Menu';
import Navbar from './components/Navbar';
import Bills from './components/Bills';
export class App extends Component {
  constructor() {
    super();
    this.state = {
      foodItem: [
        {
          id: 1,
          item: 'Chicken Biryani',
          price: 250,
          isFavorite: false,
        },
        {
          id: 2,
          item: 'Mutton Biryani',
          price: 350,
          isFavorite: false,
        },
        {
          id: 3,
          item: 'Veg Biryani',
          price: 150,
          isFavorite: false,
        },
        {
          id: 4,
          item: 'Chicken Masala',
          price: 180,
          isFavorite: false,
        },
        {
          id: 5,
          item: 'Butter Chicken',
          price: 220,
          isFavorite: false,
        },
        {
          id: 6,
          item: 'Chicken chilli',
          price: 130,
          isFavorite: false,
        },
      ],
      orderedFood: [],
    };
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick = (id) => {
    this.setState({
      orderedFood: this.state.foodItem
        .map((data) => {
          if (data.id === id) {
            data.isFavourite = !data.isFavourite;
          }
          return data;
        })
        .filter((data) => data.isFavourite === true),
    });
  };
  render() {
    return (
      <Router>
        <div>
          <Navbar />
          <Route
            exact
            path="/"
            component={() => (
              <Menu
                foodItem={this.state.foodItem}
                handleClick={this.handleClick}
              />
            )}
          />
          <Route
            exact
            path="/bills"
            component={() => <Bills orderedFood={this.state.orderedFood} />}
          />
        </div>
      </Router>
    );
  }
}

export default App;
