import React, { Component } from 'react';
import { Link } from 'react-router-dom';
export class Bills extends Component {
  render() {
    const { orderedFood } = this.props;
    return (
      <div className="menu-container">
        {orderedFood.map((data) => (
          <div className="item-container" key={data.id}>
            <p>{data.item}</p>
            <p>{data.price} Rs</p>
          </div>
        ))}
        <div>
          <p>
            Total Bill :
            {Object.values(orderedFood).reduce((t, { price }) => t + price, 0) +
              Math.floor(
                Object.values(orderedFood).reduce(
                  (t, { price }) => t + price,
                  0
                ) / 10
              )}{' '}
            Rs (10% tip)
          </p>
          <p>
            Enjoying the food, order more{' '}
            <Link to="/">
              <small>here</small>
            </Link>
          </p>
        </div>
      </div>
    );
  }
}

export default Bills;
